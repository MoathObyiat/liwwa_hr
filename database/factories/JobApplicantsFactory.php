<?php

use Faker\Generator as Faker;

$factory->define(App\JobApplicants::class, function (Faker $faker) {
    return [
        'full_name' => $faker->name,
        'date_of_birth' => $faker->dateTimeBetween('1970-01-01', '2012-12-31')->format('Y-m-d'),
        'years_of_experience' => rand(1,25),
        'department_id' => rand(1,3),
        'resume' => '/MoathMobarakProfile.pdf',
    ];
});