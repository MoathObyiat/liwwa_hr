<?php

use Illuminate\Database\Seeder;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'id' => '1',
            'name' => 'IT',
            'created_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('departments')->insert([
            'id' => '2',
            'name' => 'HR',
            'created_at' => date("Y-m-d H:i:s")
        ]);

        DB::table('departments')->insert([
            'id' => '3',
            'name' => 'Finance',
            'created_at' => date("Y-m-d H:i:s")
        ]);
    }
}
