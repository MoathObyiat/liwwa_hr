<?php

namespace App\Http\Controllers;

use Response;
use App\User;
use App\JobApplicants;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    
    public function index()
    {
        $applicants = JobApplicants::orderBy('id', 'DESC')->get();
        $recourd_list = $this->get_recourd_list();
        return view('dashboard.index', compact('recourd_list', 'applicants'));
    }

    public function download($id)
    {
        $applicant = JobApplicants::where('id', $id)->first();
        $file = public_path() . $applicant->resume;

        return Response::download($file, $applicant->full_name);
    }

    function get_recourd_list(){
        
        $users_count = User::count();
        $applicants_count  = JobApplicants::count();
        return $recourd_list = array(
            
            'users'=> array(
                'route' =>   'users.index',
                'title' =>   'Users',
                'count' =>   $users_count,
                'class' =>   'mini-charts-item bgm-teal',
                'diff'  =>   5 - $users_count,
                'slug'  =>   'users'
                ),
                'logs'=> array(
                    'route' =>   'dashboard',
                    'title' =>   'Job Applicants',
                    'count' =>   $applicants_count,
                    'class' =>   'mini-charts-item bgm-indigo',
                    'diff'  =>   5 - $applicants_count,
                    'slug'  =>   'logs'
                ),
        );
           
    }

}