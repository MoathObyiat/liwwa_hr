<?php

namespace App\Http\Controllers;

use  App\Departments;
use  App\JobApplicants;
use Illuminate\Http\Request;
use App\Http\Requests\JobApplicantsRequest;

class JobApplicantsController extends Controller
{
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Departments::all()->pluck('name', 'id');
        return view('applicants.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobApplicantsRequest $request)
    {
        $applicants = JobApplicants::create($request->all());

        $image       = $request->file('resume');
        $resume  = '';

        if($request->hasFile('resume'))
            $resume = upload_image("job_applicants" , $image , "create" , $applicants->resume);
        
        JobApplicants::findOrFail($applicants->id)->update([
            'resume' => ($resume != '') ? $resume : $image,
        ]);

        flash()->success('Job Applicant submitted successfully!');
        return redirect(route('jobApplicants'));
    }

    public function storeTest(Request $request)
    {
        // dd($request->all());
        $applicants = JobApplicants::create($request->all());
        return response()->json($applicants, 201);
    }
}
