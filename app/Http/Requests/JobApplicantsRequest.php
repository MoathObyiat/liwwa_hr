<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobApplicantsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required',
            'date_of_birth' => 'required|before:today',
            'years_of_experience' => 'required',
            'department_id' => 'required',
            'resume'  => 'required|mimes:pdf,docx',
        ];
    }
}
