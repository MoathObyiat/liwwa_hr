<?php

function upload_image($folder , $image , $type , $file = NULL){

    $folder   = Config::get('app.files') .'/' .$folder.'/' ;
    $public = public_path() .'/';

    if($type == "update" && $file != Null) {
        if(File::exists($file))
            unlink($file);
    }

    if(!File::exists($public . $folder)) {
       File::makeDirectory($public . $folder);
    }
    
    $image_name   = time().'_'.$image->getClientOriginalName();
    $image_upload = $image->move($public.$folder , $image_name);
    return  $folder.$image_name;  
}

/*
* retrieve current used languages 
*/
function get_lang(){
    return Lang::locale();

}