<?php

namespace App;

use Auth;
use Route;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
        'last_name',
        'first_name',
        'status',
        'phone_1'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot() {
        parent::boot();

        if(Route::getCurrentRoute()->Uri() !== 'password/reset'){
            // create a event to happen on updating
            static::updating(function($table)  {
                $table->updated_by = Auth::user()->username;
            });

            // create a event to happen on saving
            static::creating(function($table)  {
                $table->created_by = Auth::user()->username;
            });
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MyResetPassword($token));
    }
}
