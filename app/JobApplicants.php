<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplicants extends Model
{
    protected $table = 'job_applicants';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'full_name',
        'date_of_birth',
        'years_of_experience',
        'department_id',
        'resume'
    ];

        /**
     * user() many-to-many relationship method
     *
     * @return QueryBuilder
     */
    public function department()
    {
        return $this->belongsTo('App\Departments', 'department_id');
    }
}
