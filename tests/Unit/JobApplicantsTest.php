<?php

namespace Tests\Unit;

use  App\JobApplicants;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JobApplicantsTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    // public function testExample()
    // {
    //     $this->assertTrue(true);
    // }

    public function test_can_create_job_applicant() {
        // $data = [
        //     'full_name' => 'moath',
        //     'date_of_birth' =>'1990-07-06',
        //     'years_of_experience' =>  5,
        //     'department_id' =>  3,
        //     'resume' =>  '/MoathMobarakProfile.pdf',
        // ];
        // dd($this->faker->dateTimeBetween('1970-01-01', '2012-12-31')->format('Y-m-d'));
        $data = [
            'full_name' => $this->faker->name,
            'date_of_birth' => $this->faker->dateTimeBetween('1970-01-01', '2012-12-31')->format('Y-m-d'),
            'years_of_experience' => rand(1,25),
            'department_id' => rand(1,3),
            'resume' => '/MoathMobarakProfile.pdf',
        ];
        
        $this->post(route('applicantsTest'), $data)
            ->assertStatus(201)
            ->assertJson($data);
    }
}
