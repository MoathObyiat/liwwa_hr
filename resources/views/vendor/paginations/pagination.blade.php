  <style>
.paginate {
    display: inline !important;
    font-size: 15px;

}
.paginate a {
    color: #a2a2a4 !important;
    float: left !important;
    padding-top: 0px;
    padding-left: 4px;
    padding-right: 4px;
    padding-bottom: 13px;
    text-decoration: none !important;
}
.paginate  li.active a {
    background-color: #c2cb25;
    color: #FFFFFF;

}
.paginate li {
    float: left;
    left: 47%;
    position: relative;
}

.paginate li a {
    position: relative !important;
    text-align: center !important;
}
.paginate a:hover:not(.active) {
    background-color: white !important;
}
</style>

@if ($paginator->lastPage() > 1)
    <ul class="paginate">
        <li class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url($paginator->currentPage()-1) }}"> <img src="/images/gray-left-arrow.png"> </a>
         </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
            $total_links = 2;
            $from = $paginator->currentPage() - $total_links;
            $to = $paginator->currentPage() + $total_links;
            if ($paginator->currentPage() < $total_links) {
               $to += $total_links - $paginator->currentPage();
            }
            if ($paginator->lastPage() - $paginator->currentPage() < $total_links) {
                $from -= $total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
            }
            ?>
            @if ($from < $i && $i < $to)
                <li class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        @if($paginator->currentPage() != $paginator->lastPage())
        <li>
            <a href="{{ $paginator->url($paginator->currentPage()+1) }}"> <img src="/images/gray-right-arrow.png"> </a>
        </li>
        @endif
    </ul>
@endif
