<div class="card-body card-padding">

    @include('partials.form-errors')
    
    {!! csrf_field() !!}
    
    <!-- full_name, date_of_birth -->
    <div class="form-group">
        {!! Form::label('full_name', 'Full Name', ['class' => 'col-sm-2 control-label required']) !!}
        <div class="col-sm-4">
            <div class="fg-line">
                {!! Form::text('full_name', null, ['class' => 'form-control input-sm']) !!}
            </div>
        </div>

        {!! Form::label('date_of_birth', 'Date of Birth', ['class' => 'col-sm-2 control-label  required']) !!}
        <div class="col-sm-4">
            <div class="fg-line">
              {!! Form::text('date_of_birth', null, ['class' => 'form-control input-sm date-picker']) !!}
            </div>
        </div>

    </div>

      
      <!-- years_of_experience, department_id -->
      <div class="form-group"> 
            {!! Form::label('years_of_experience', 'Years of Experience', ['class' => 'col-sm-2 control-label required']) !!}
            <div class="col-sm-4">
                <div class="fg-line">
                    {!! Form::number('years_of_experience', null, ['class' => 'form-control input-sm', 'min' => 0]) !!}
                </div>
            </div>

            {!! Form::label('department_id', 'Department', ['class' => 'col-sm-2 control-label required']) !!}
            <div class="col-sm-4">
                <div class="fg-line">
                    {!! Form::select('department_id', $departments ,null, ['class' => 'form-control input-sm selectpicker']) !!}
                </div>
            </div>
      </div>

      <div class="form-group">
        {!! Form::label('resume', 'Resume', ['class' => 'col-sm-2 control-label , required']) !!}
        <div class="col-sm-4">
            <div class="form-control input-sm">
                <input type="file" name="resume"/>
            </div>
        </div>
        
        </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary btn-sm">@lang('common.save')</button>
      </div>
    </div>
    
</div>