@extends('home')

@section('content')
    <div class="card">

        {!! Form::open(['method' => 'POST', 'url' => route('applicants'), 'class' => 'form-horizontal', 'files' => true]) !!}
            <div class="card-header">
                <h2>Job Applicants</h2>
            </div>

            @include('applicants.form')
        {!! Form::close() !!}
    </div>
@endsection
