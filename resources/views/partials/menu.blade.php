<aside id="sidebar">
    <div class="sidebar-inner">
        <div class="s-profile">
            <a href="{{ url('dashboard') }}" data-ma-action="profile-menu-toggle">
                <div class="sp-pic">
                </div>
            </a>
        </div>
        <div class="si-inner">
            <ul class="main-menu">
                <li>
                    <a href="{{route('dashboard')}}"><i class="md md-home"></i>@lang('menu.dashboard')
                    </a>
                </li>
               
                <li>
                    <a href="{{route('users.index')}}"><i class="md md-people"></i>@lang('menu.users')
                    </a>
                </li>
            </ul>
        </div>
    </div>
</aside>