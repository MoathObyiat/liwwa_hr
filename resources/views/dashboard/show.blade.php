@extends('app')

@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-face"></i>View #{{ $log->id }}</h2> 
    </div>


    <div class="card">
        <div class="card-body card-padding">
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-account-o zmdi-hc-fw"></li> User ID:</dt>
                        <dd>{{ $log->User_ID }}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-account-o infinite wobble zmdi-hc-fw"></li> User Type:</dt>
                        <dd>{{ $log->User_Type }}</dd>
                    </dl>
                    
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-file infinite wobble zmdi-hc-fw"></li> App Version:</dt>
                        <dd>{{ $log->App_Version }}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-tv infinite wobble zmdi-hc-fw"></li> Platform:</dt>
                        <dd>{{ $log->Platform }}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-tv infinite wobble zmdi-hc-fw"></li>Device Model:</dt>
                        <dd>{{ $log->Device_Model }}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-code infinite wobble zmdi-hc-fw"></li>Model Code:</dt>
                        <dd>{{ $log->Model_Code }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-comment infinite wobble zmdi-hc-fw"></li>Error Message:</dt>
                        <dd>{{ $log->Error }}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-comment infinite wobble zmdi-hc-fw"></li>Stack:</dt>
                        <dd>{{ $log->Stack }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-comment infinite wobble zmdi-hc-fw"></li>Components Stack:</dt>
                        <dd>{{ $log->Components_Stack }}</dd>
                    </dl>
                    
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-pin infinite wobble zmdi-hc-fw"></li>Location:</dt>
                        <dd>{{ $log->Location }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-pin infinite wobble zmdi-hc-fw"></li>Path:</dt>
                        <dd>{{ $log->Path }}</dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt><li class="zmdi zmdi-time infinite wobble zmdi-hc-fw"></li>Created At:</dt>
                        <dd>{{ $log->created_at }}</dd>
                    </dl>

                    <div class="form-group">
                        <div class="col-sm-12">
                            <a class="btn btn-info btn-sm" href="{{ route('dashboard') }}">Back</a>
                        </div>
                    </div>
                    
                   
                </div>
            </div>
        </div>
    </div>
@endsection
