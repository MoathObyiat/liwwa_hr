        
<!-- counters  -->
    <div class="row">
        @foreach($recourd_list as $key => $val)
            
            <div class="col-sm-6 col-sm-4">
                <div class="mini-charts-item {{ $val['class'] }}">
                    <div class="clearfix">

                        <div class="chart stats-bar"><canvas style="display: inline-block; width: 68px; height: 35px; vertical-align: top;" width="68" height="35"></canvas></div>
                        <a href="{{ route($val['route']) }}">
                            <div class="count">
                                <strong>{{ $val['title'] }}</strong>
                                <h2>{{ $val['count'] }}</h2>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>