@extends('app')
@section('js')
    var datatable = $("#data-table-command").DataTable({
        "order": [[ 1, "desc" ]], 
    });
@endsection

@section('content')

    <div class="block-header">
        <h2><i class="zmdi zmdi-view-dashboard zmdi-hc-fw"></i> @lang('dashboard.page_title')</h2>
        <div class="container">
        <div class="row">&nbsp</div>
            @include('dashboard.partials_block')

            <ul class="actions">
                <li>
                </li>
            </ul>
        </div>
    </div>
    
    <div class="card">
        <div class="card-header">
        </div>

        <table id="data-table-command" class="uk-table uk-table-hover uk-table-striped" cellspacing="0" >
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Full Name</th>
                    <th>Date of birth</th>
                    <th>Years of Experience</th>
                    <th>Department</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applicants as $applicant)
                
                    <tr>
                        <td>{{ $applicant->id }}</td>
                        <td>{{ $applicant->full_name }}</td>
                        <td>{{ $applicant->date_of_birth }}</td>
                        <td>{{ $applicant->years_of_experience }}</td>
                        <td>{{ $applicant->department->name }}</td>
                        <td class="td-spical">
                            <a class="btn btn-icon command-edit" href="{{ route('dashboard.download', $applicant->id) }}"  data-toggle="tooltip" data-placement="top" data-original-title="Download Resume"><i class="glyphicon mk glyphicon-download"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection