<?php

// Users CRUD
Route::get('users', [
    'uses'       => 'UsersController@index',
    'as'         => 'users.index',
    'permission' => 'users',
    'menuItem'   => ['icon' => 'md md-people', 'title' => 'Users Management']
]);

Route::get('users/create', [
    'uses'       => 'UsersController@create',
    'as'         => 'users.create',
    'permission' => 'users'
]);

Route::post('users', [
    'uses'       => 'UsersController@store',
    'as'         => 'users.store',
    'permission' => 'users'
]);

Route::get('users/{id}/edit', [
    'uses'       => 'UsersController@edit',
    'as'         => 'users.edit',
    'permission' => 'users'
]);

Route::put('users/{id}', [
    'uses'       => 'UsersController@update',
    'as'         => 'users.update',
    'permission' => 'users'
]);

Route::post('users/{id}/delete', [
    'uses'       => 'UsersController@destroy',
    'as'         => 'users.destroy',
    'permission' => 'users'
]);

Route::get('users/{id}/track', [
    'uses'       => 'UsersController@track',
    'as'         => 'users.track',
    'permission' => 'users'
]);