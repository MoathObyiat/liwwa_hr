<?php

// Dashboard
Route::get('locale/{locale}', [
    'uses' 		 => 'DashboardController@changeLocale',
    'as' 		 => 'dashboard.changeLocale'
]);

Route::get('download/{id}', [
    'uses' 		 => 'DashboardController@download',
    'as' 		 => 'dashboard.download'
]);

