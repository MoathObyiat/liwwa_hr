<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('applicants', [
    'uses' => 'JobApplicantsController@index',
    'as' => 'jobApplicants'
]);
Route::post('applicants', [
    'uses' => 'JobApplicantsController@store',
    'as' => 'applicants'
]);
Route::post('applicantsTest', 'JobApplicantsController@storeTest')->name('applicantsTest');

Auth::routes();

App::setLocale( Request::segment(1) != null ? Request::segment(1) : 'en' );

Route::group(['prefix' => App::getLocale()], function () {

    // Protected Routes by auth and acl middleware
    Route::group(['middleware' => ['web', 'auth']], function () {
        
        Route::get('dashboard', [
            'uses' => 'DashboardController@index',
            'as' => 'dashboard',
            'permission' => 'dashboard',
            'menuItem' => ['icon' => 'md md-dashboard', 'title' => 'Dashboard']
        ]);

        // Dynamically include all files in the routes directory
        foreach (new DirectoryIterator(__DIR__ . '/') as $file)
        { 
            if (!$file->isDot() && !$file->isDir() && $file->getFilename() != '.gitignore')
            {
                require_once __DIR__ . '/' . $file->getFilename();
            }
        }
        
    });
});

