#How to install Liwwa HR from the GIT

- Clone the project
- checkout intended branch "master"
- duplicate the `.env.example` and rename it to `.env`
- Edit the `.env` file according to required settings (db..etc)
- in the CLI, type `composer install`
- in the CLI, type `composer update`
- in the CLI, type `php artisan key:generate`
- then type `php artisan migrate --seed`
⋅⋅* When you need to add data to a predefined table, create a new seeder by typing `php artisan make:seed NewRoleSeeder` and send it to the database using `php artisan db:seed --class=NewRoleSeeder`

#How To update Liwwa HR

--* in the cli type `composer dump-autoload`
--* in the cli type `php artisan migrate:refresh --seed`

# Serve App:

- run application: `php artisan serve`
- for candidates: `http://localhost:8000/applicants`
- for Admin: `http://localhost:8000/login`

- default account generate using Migration & Seeder:
- email: `moathobyiat@gmail.com` password: `123456`



#Installation

#Server Requirements
The Laravel framework has a few system requirements. All of these requirements are satisfied by the Laravel Homestead virtual machine, so it's highly recommended that you use Homestead as your local Laravel development environment.

However, if you are not using Homestead, you will need to make sure your server meets the following requirements:

- PHP >= 7.2.0
- BCMath PHP Extension
- Ctype PHP Extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

# Please ceheck Unit Test:
- go test folder -> Unit folder -> `JobApplicantsTest.php`
..* Note: you need install phpunit for check Automated Tests. 

# Pleas checek this site if need more info: 
- `https://laravel.com/docs/master/installation`
# Or contact me:
- `moathobyiat@gmail.com`
- 0786422213